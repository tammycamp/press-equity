# Press Equity

**Let’s Fix the Tech ‘Press Equity’ Algorithm**

We shape our world with code. Journalists shape our world with words. 

As a fintech founder I’ve been observing how press coverage impacts startups. What gets written about gets funded, and what gets funded gets built. 

I’ve heard—not all, but many—VCs express the best of intentions about inclusion. I’ve seen my talented female and BIPOC tech peers struggle for recognition and capital. 

Then look at this [outcome](https://pitchbook.com/news/articles/vc-funding-female-founders-drops-low). What scripting has left so many of us on the margins? 

There’s a venture funding feedback loop that keeps reinforcing the familiar.

We could rage against these mechanisms, but it’s probably more productive to find the broken bit of code. 

I have a theory: There’s a bug in the tech industry press process. 

I’m embarking on a project to address “press equity,” to tell the story of who gets their story told. 

I’m going to drive this with data, by quantifying the trends in tech press coverage. [Tracy Chou](https://en.wikipedia.org/wiki/Tracy_Chou)—the Pinterest engineer who led a groundbreaking project to extract and analyze big tech hiring data—serves as my inspiration and model. 

I invite you, my technologist peers, to collaborate in developing a similar [open sourced GitHub repository](https://github.com/tammycamp/press-equity). 

We’ll create software to mine data on each business publication’s coverage inclusivity, interpret the data, and display the results. 

I welcome and value your contributions to building our more equitable future.


**Proof of Concept**

The proof of concept should take the Twitter API and understand who are verified accounts in the business sector.  We would analyze those verified accounts for diversity metrics like gender and people of color.  Then we would take real world data and analyze if it equates to what is reflected in the business world.
To start, this project should segment Twitter verified accounts by category of "business", "verified", and "gender".

What is the result in percentages?

From Twitter we would need to "get all" users of "verified=true"

https://developer.twitter.com/en/docs/labs/tweets-and-users/overview

I'm not sure if this is possible, but that would be cool if so.
